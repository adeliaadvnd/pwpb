<?php

    class Controller {
        public function model($model) {
            require_once("model/{$model}.php");
            return new $model;
        }

        public function view($view, $params = []) {
            require_once("view/{$view}.php");
        }
    }

?>